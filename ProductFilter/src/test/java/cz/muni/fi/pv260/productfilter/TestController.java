package cz.muni.fi.pv260.productfilter;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Rule;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyCollectionOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test suite for Controller
 */
public class TestController {

    // CLASS ATTRIBUTES

    private List<Product> testProducts;
    private Controller controller;

    // Mock definitions
    private Input mockInput = mock(Input.class);
    private Logger mockLogger = mock(Logger.class);
    private Output mockOutput = mock(Output.class);

    // RULES

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUpTest() {
        testProducts = Arrays.asList(
            new Product(1, "apple", Color.RED, BigDecimal.ONE),
            new Product(2, "lamborghini", Color.RED, BigDecimal.TEN),
            new Product(3, "siamese cat hair", Color.BLACK, BigDecimal.ZERO)
        );

        try {
            when(mockInput.obtainProducts()).thenReturn(testProducts);
        } catch (ObtainFailedException ex) {

        }

        controller = new Controller(mockInput, mockOutput, mockLogger);
    }

    // ILLEGALARGUMENTEXCEPTION TESTS

    @Test
    public void testNullInputThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        controller = new Controller(null, mockOutput, mockLogger);
    }

    @Test
    public void testNullOutputThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        controller = new Controller(mockInput, null, mockLogger);
    }

    @Test
    public void testNullLoggerThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        controller = new Controller(mockInput, mockOutput, null);
    }

    // NULLPOINTEREXCEPTION TESTS

    @Test
    public void testSelectNullFilterThrowsNullPointerException() {
        expectedException.expect(NullPointerException.class);
        controller.select(null);
    }

    // SELECTION FILTER MATCH TESTS

    @Test
    public void testSelectWithRedColorFilterReturnsRedProducts() {
        controller.select(new RedColorFilter());

        List<Product> resultExpected = new ArrayList<>(testProducts);
        resultExpected.remove(testProducts.get(2));
        verify(mockOutput).postSelectedProducts(resultExpected);
    }

    @Test
    public void testSelectWithNotLamborghiniFilterReturnsNotLamborghinis() {
        controller.select(new NotLamborghiniFilter());

        List<Product> resultExpected = new ArrayList<>(testProducts);
        resultExpected.remove(testProducts.get(1));
        verify(mockOutput).postSelectedProducts(resultExpected);
    }

    @Test
    public void testSelectWithIdLessThanTwoFilterReturnsApple() {
        controller.select(new IdLessThanTwoFilter());

        List<Product> resultExpected = new ArrayList<>(testProducts);
        resultExpected.remove(testProducts.get(1));
        resultExpected.remove(testProducts.get(2));
        verify(mockOutput).postSelectedProducts(resultExpected);
    }

    @Test
    public void testSelectWithDontPassFilterReturnsEmptyCollection() {
        controller.select(new DontPassFilter());
        verify(mockOutput).postSelectedProducts(new ArrayList<Product>());
    }

    @Test
    public void testSelectWithAlwaysPassFilterReturnsTestProducts() {
        controller.select(new AlwaysPassFilter());
        verify(mockOutput).postSelectedProducts(testProducts);
    }

    // LOGGING TESTS

    @Test
    public void testSelectLogsInfoOnSuccess() {
        controller.select(new DontPassFilter());
        String expectedMessage = String.format("Successfully selected %d out of %d available products.", 0, 3);
        verify(mockLogger, times(1)).setLevel("INFO");
        verify(mockLogger).log(Controller.class.getSimpleName(), expectedMessage);

        controller.select(new IdLessThanTwoFilter());
        expectedMessage = String.format("Successfully selected %d out of %d available products.", 1, 3);
        verify(mockLogger, times(2)).setLevel("INFO");
        verify(mockLogger).log(Controller.class.getSimpleName(), expectedMessage);

        controller.select(new NotLamborghiniFilter());
        expectedMessage = String.format("Successfully selected %d out of %d available products.", 2, 3);
        verify(mockLogger, times(3)).setLevel("INFO");
        verify(mockLogger).log(Controller.class.getSimpleName(), expectedMessage);

        controller.select(new AlwaysPassFilter());
        expectedMessage = String.format("Successfully selected %d out of %d available products.", 3, 3);
        verify(mockLogger, times(4)).setLevel("INFO");
        verify(mockLogger).log(Controller.class.getSimpleName(), expectedMessage);
    };

    @Test
    public void testSelectLogsErrorOnObtainFailedException() {
        try {
            when(mockInput.obtainProducts()).thenThrow(ObtainFailedException.class);
        } catch (ObtainFailedException ex) {

        }

        controller.select(new AlwaysPassFilter());

        String expectedMessage = String
            .format("Filter procedure failed with exception: %s", ObtainFailedException.class.getName());

        verify(mockLogger, times(1)).setLevel("ERROR");
        verify(mockLogger).log(Controller.class.getSimpleName(), expectedMessage);
    }

    @Test
    public void testSelectPassesNothingOnObtainFailedException() throws ObtainFailedException {
        try {
            when(mockInput.obtainProducts()).thenThrow(ObtainFailedException.class);
        } catch (ObtainFailedException ex) {

        }

        controller.select(new AlwaysPassFilter());

        verify(mockOutput, never()).postSelectedProducts(anyCollectionOf(Product.class));
    }

    // MANUAL TEST DOUBLE CLASS DEFINITIONS

    private class RedColorFilter implements Filter<Product> {

        @Override
        public boolean passes(Product product) {
            return product.getColor() == Color.RED;
        }
    }

    private class NotLamborghiniFilter implements Filter<Product> {

        @Override
        public boolean passes(Product product) {
            return product.getName() != "lamborghini";
        }
    }

    private class IdLessThanTwoFilter implements Filter<Product> {

        @Override
        public boolean passes(Product product) {
            return product.getId() < 2;
        }
    }

    private class DontPassFilter implements Filter<Product> {

        @Override
        public boolean passes(Product product) {
            return false;
        }
    }

    private class AlwaysPassFilter implements Filter<Product> {

        @Override
        public boolean passes(Product product) {
            return true;
        }
    }
}