package cz.muni.fi.pv260.productfilter;

import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Rule;

import static org.junit.Assert.*;

/**
 * Test suite for AtLeastNOfFilter
 */
public class TestAtLeastNOfFilter {

    // CLASS ATTRIBUTES

    private Object testObject = new Object();

    // Manual test doubles
    private Filter<Object> filterIsThatTestObject = new FilterIsThatObject(testObject);
    private Filter<Object> filterAlwaysPasses = new FilterAlwaysPass();
    private Filter<Object> filterDoesNotPass = new FilterDoesNotPass();

    // RULES

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    // VALID CONSTRUCTION TESTS

    @Test
    public void testAllowedConstructionPasses() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1, filterAlwaysPasses);
        assertNotNull(filter);

        filter = new AtLeastNOfFilter(1, filterAlwaysPasses, filterDoesNotPass);
        assertNotNull(filter);

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterDoesNotPass);
        assertNotNull(filter);

        filter = new AtLeastNOfFilter(1, filterAlwaysPasses, filterDoesNotPass, filterIsThatTestObject);
        assertNotNull(filter);

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterIsThatTestObject, filterDoesNotPass);
        assertNotNull(filter);

        filter = new AtLeastNOfFilter(3, filterIsThatTestObject, filterDoesNotPass, filterAlwaysPasses);
        assertNotNull(filter);
    }

    // ILLEGALARGUMENTEXCEPTION THROWN TESTS

    @Test
    public void testZeroNThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(0);
    }

    @Test
    public void testZeroNWithFilterThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(0, filterAlwaysPasses);
    }

    @Test
    public void testNegative1NThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-1);
    }

    @Test
    public void testNegative1NWithFilterThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-1, filterAlwaysPasses);
    }

    @Test
    public void testNegative2NThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-2);
    }

    @Test
    public void testNegative2NWithFilterThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-2, filterAlwaysPasses);
    }

    @Test
    public void testNegative422NThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-422);
    }

    @Test
    public void testNegative422WithFiltersThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(-422, filterAlwaysPasses, filterAlwaysPasses);
    }

    // FILTERNEVERSUCCEEDS THROWN TESTS

    @Test
    public void testNoFilter1NThrowsFilterNeverSucceeds() {
        expectedException.expect(FilterNeverSucceeds.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1);
    }

    @Test
    public void testNoFilter2NThrowsFilterNeverSucceeds() {
        expectedException.expect(FilterNeverSucceeds.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(2);
    }

    @Test
    public void test1Filter2NThrowsFilterNeverSucceeds() {
        expectedException.expect(FilterNeverSucceeds.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(2, filterAlwaysPasses);
    }

    @Test
    public void testNoFilter63NThrowsFilterNeverSucceeds() {
        expectedException.expect(FilterNeverSucceeds.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(63);
    }

    @Test
    public void test3Filter63NThrowsFilterNeverSucceeds() {
        expectedException.expect(FilterNeverSucceeds.class);
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(
            63, filterAlwaysPasses, filterDoesNotPass, filterIsThatTestObject
        );
    }

    // FILTER PASSES TESTS

    @Test
    public void testPassesWhenAllPass() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1, filterAlwaysPasses);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterAlwaysPasses);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterAlwaysPasses, filterIsThatTestObject);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            4,
            filterAlwaysPasses,
            filterAlwaysPasses,
            filterIsThatTestObject,
            filterIsThatTestObject,
            filterIsThatTestObject
        );
        assertTrue(filter.passes(testObject));
    }

    @Test
    public void testPassesWhenExactlyNPass() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1, filterAlwaysPasses, filterDoesNotPass);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterAlwaysPasses, filterDoesNotPass);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            3,
            filterAlwaysPasses,
            filterDoesNotPass,
            filterIsThatTestObject,
            filterDoesNotPass,
            filterIsThatTestObject
        );
        assertTrue(filter.passes(testObject));
    }

    @Test
    public void testPassesWhenAtLeastNPass() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1, filterAlwaysPasses, filterDoesNotPass);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterAlwaysPasses, filterAlwaysPasses);
        assertTrue(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            3,
            filterAlwaysPasses,
            filterAlwaysPasses,
            filterIsThatTestObject,
            filterIsThatTestObject,
            filterIsThatTestObject
        );
        assertTrue(filter.passes(testObject));
    }

    // FILTER FAILS TESTS

    @Test
    public void testFailsWhenAllFail() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(1, filterDoesNotPass, filterDoesNotPass);
        assertFalse(filter.passes(testObject));

        filter = new AtLeastNOfFilter(1, filterDoesNotPass, filterDoesNotPass, filterDoesNotPass);
        assertFalse(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            1,
            filterDoesNotPass,
            filterDoesNotPass,
            filterDoesNotPass,
            filterDoesNotPass,
            filterDoesNotPass
        );
        assertFalse(filter.passes(testObject));
    }

    @Test
    public void testFailsWhenNMinus1Pass() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(2, filterAlwaysPasses, filterDoesNotPass);
        assertFalse(filter.passes(testObject));

        filter = new AtLeastNOfFilter(3, filterAlwaysPasses, filterAlwaysPasses, filterDoesNotPass);
        assertFalse(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            4,
            filterAlwaysPasses,
            filterDoesNotPass,
            filterAlwaysPasses,
            filterAlwaysPasses,
            filterDoesNotPass
        );
        assertFalse(filter.passes(testObject));
    }

    @Test
    public void testFailsWhenLessThanNMinus1Pass() {
        AtLeastNOfFilter<Object> filter = new AtLeastNOfFilter(
            3, filterAlwaysPasses, filterDoesNotPass, filterDoesNotPass
        );
        assertFalse(filter.passes(testObject));

        filter = new AtLeastNOfFilter(
            4,
            filterDoesNotPass,
            filterDoesNotPass,
            filterDoesNotPass,
            filterAlwaysPasses,
            filterDoesNotPass
        );
        assertFalse(filter.passes(testObject));
    }

    // MANUAL TEST DOUBLE CLASS DEFINITIONS

    private class FilterAlwaysPass implements Filter<Object> {

        @Override
        public boolean passes(Object object) {
            return true;
        }
    }

    private class FilterDoesNotPass implements Filter<Object> {

        @Override
        public boolean passes(Object object) {
            return false;
        }
    }

    private class FilterIsThatObject implements Filter<Object> {

        private Object thatObject;

        public FilterIsThatObject(Object thatObject) {
            this.thatObject = thatObject;
        }

        @Override
        public boolean passes(Object object) {
            return object == thatObject;
        }
    }
}
