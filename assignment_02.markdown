# 2.2 Task 2

Your team is about to implement software for e-shop with ambitions to be the biggest e-shop in Czech Republic.
Identify at least:
 
* 7 attributes which you think are the most relevant for this product to be successful.
	1. user-friendliness
	2. cross-device
	3. scalability (with increasing number of products)
	4. security (payment information,...)
	5. efficiency (searching, comparing, finding alternatives)
	6. accessibility
	7. availability
* 15 components which are (almost) necessary for the product to be successful. (use clustering and think not only about user-visible components)
	1. Database
	2. Product page
	3. Cart
	4. User settings
	5. Analytics
	6. CMS
	7. Error logging
	8. Sign Up & Login
	9. Search
	10. Loyalty program
	11. Reliable servers
	12. Payment gateway
	13. EET integration
	14. Order tracking
	15. Product filtering
* 10 capabilities
	1. 2-step verification possibility
	2. 1-click payment
	3. Registration is over HTTPS protocol.
	4. Passwords are hashed with salt before being saved to database.
	5. There are related (cross-sell) and similar (upsell) products shown on each product page.
	6. Web is responsive and scales well to 95 % of most-commonly used screen resolutions.
	7. Web is localized to other languages than just Czech
	8. Products from the same category can be compared in a table-view and simple comparison
	   indicators.
	9. User can choose to get notified when an out-of-stock product gets restocked.
	10. Discussions about products.
* 5 test cases
	1. When adding a quantity higher than currently in stock (of any given product) to cart, an error
	   message is shown and products are not added to cart.
	2. Adding a non-existent delivery address results in an error message.
	3. Registering with an e-mail address that is of wrong format results in an error.
	4. When a user is registered, confirmation URL is generated and sent in an e-mail. When that URL
	   is not visited in a set period of time. Registration is deleted.
	5. After placing an order, user is informed via e-mail / SMS.
* 8 of the most risky clusters of impact (e.g. one cluster of impact can be “not getting existing customers to buy more of our stuff”).
	1. Server unavailability (DDoS, electricity issues,...)
	2. Data leaks
	3. Lack of personnel
	4. Package delivery issues
	5. Wrong product info
	6. Customers returning packages
	7. Competition having better features (lower prices, faster shipping,...)
	8. Poor customer-acquisition marketing

**Explain why the attributes and clusters of impact you chose are of the highest importance.**

Attributes:
First of all, potentional customers need to want to use this product in order for it to have an
opportunity to work. The product has to be technicaly functional in order to generate conversions
from people who want to use it.

Clusters of impact can have following consequences:
* they can make service completely unavailable (1)
* have legal consequences (2)
* have great financial impact (they need to be pre-planned for (4, 6))
* can make the e-shop lose trust in our product and harm product's brand and integrity

How did you minimize chances that there are other attributes or clusters of impact you don't see, but are of higher importance than those you wrote down?

First of all, I couldn't think of higher importance than having a service running without facing
legal consequences for a job done wrong, as that is something I consider to be basics.
Other than that, I actually asked my roommates with varying point of views to help me broaden my perception of the issue.
