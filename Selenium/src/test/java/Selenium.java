import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class Selenium {

    @Test
    public void testClingonLanguageNotPresentOnWikipedia() {
        System.setProperty("webdriver.chrome.driver", "D:\\downloadz\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://cs.wikipedia.org");
        boolean clingonFound = false;

        WebElement languagesList = webDriver.findElement(By.id("p-lang"))
                                            .findElement(By.cssSelector("div.body"))
                                            .findElement(By.tagName("ul"));

        List<WebElement> languages = languagesList.findElements(By.tagName("li"));
        for (WebElement language : languages) {
            WebElement link = language.findElement(By.tagName("a"));
            if (link.getAttribute("title").equals("klingonština")) clingonFound = true;
        }

        try {
            assertFalse("Wikipedia can not be viewed in Clingon", clingonFound);
        } finally {
            webDriver.close();
        }
    }

    @Test
    public void logoLeadsToMainPageBatch() {
        System.setProperty("webdriver.chrome.driver", "D:\\downloadz\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        for (int i = 0; i < 10; ++i) {
            logoLeadsToMainPage(webDriver);
        }
        webDriver.close();
    }

    private void logoLeadsToMainPage(WebDriver webDriver) {
        webDriver.get("https://cs.wikipedia.org/wiki/Speci%C3%A1ln%C3%AD:N%C3%A1hodn%C3%A1_str%C3%A1nka");
        WebElement logo = webDriver.findElement(By.cssSelector("div#p-logo"))
                                   .findElement(By.tagName("a"));
        assertTrue(logo.getAttribute("href").equals("https://cs.wikipedia.org/wiki/Hlavn%C3%AD_strana"));
    }

    @Test
    public void captchaRequiredForRegistration() {
        System.setProperty("webdriver.chrome.driver", "D:\\downloadz\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://cs.wikipedia.org/w/index.php?title=Speciální:Vytvořit_účet");

        WebElement registrationForm = webDriver.findElement(By.cssSelector("form#userlogin2"));
        WebElement userNameInput = registrationForm.findElement(By.name("wpName"));
        WebElement passwordInput = registrationForm.findElement(By.name("wpPassword"));
        WebElement passwordCheckInput = registrationForm.findElement(By.name("retype"));

        userNameInput.sendKeys("foo");

        String dummyPassword = "foo_bar123";
        passwordInput.sendKeys(dummyPassword);
        passwordCheckInput.sendKeys(dummyPassword);
        passwordCheckInput.submit();

        WebElement err = webDriver.findElement(By.className("mw-htmlform-field-HTMLFancyCaptchaField"))
                                  .findElement(By.cssSelector("span.error"));

        assertTrue("Captcha required error displayed", err != null && err.getText().equals("Tato položka je povinná."));
        webDriver.close();
    }
}
