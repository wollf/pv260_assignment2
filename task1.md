# SUMMARY

The application was tested using **Google Chrome v. 74.0.3729.108**.

I don't perceive the specification as buggy, per se, but it is definitely incomplete. (see **FURTHER REQUIREMENT SPECIFICATION REQUESTS**)

Further additional testing techniques to use:

- Performance/Load testing (multiple requests at the same time)
- SQL Injection attacks (if I knew a SQL DB was connected to the service)

## FURTHER REQUIREMENT SPECIFICATION REQUESTS

- What should happen upon empty input being submitted?
- Are there any length limit requirements for `First Name` & `Last Name`?
- Are there any character type limitations for `First Name` & `Last Name`?
  - (e.g. Only alphabetical characters allowed?)
- Should there be any field validation present?
  - If so, should the submit button be disabled while validation rules are not met?
- What should happen when just one field is filled out?


## REQUIREMENT NOT MET

### Input

- First Name: **Travis**
- Second Name: **Tester**

### Expected Outcome

The line **"You are called Travis Tester. You are (140-190) cm long."** gets shown.

### Actual Outcome

The line **"Welcome Travis Tester. You are (140-190)cm long and your weight is (second_range)kg."** gets shown, where `second_range` seems to be about 60-100.


## ADDITIONAL ISSUES FOUND

### 1) Input is not sanitized for HTML code.

#### Example

##### Input

First Name - **Travis \<p\>My middle name is Jesus\<\/p\>**

Second Name - **Tester**

##### Outcome

Results in a paragraph element shown in the page. This could result in potential security issues.
This holds true for **BOTH** input fields.


### 2) Input is not sanitized for JS code.

#### Example

Try accessing:

http://qualityassurance.wz.cz/index.php?firstname=%3Cscript%3Ealert(%22I%27m%20an%20evil%20intruder!%22);&lastname=%3C/script%3E

or

- fill in First Name field with **"\<script\> \#\# YOUR JAVASCRIPT \#\#"**, where `\#\# YOUR JAVASCRIPT \#\#` is some JS code
- fill in Last Name with **"\<\/script\>"**

#### Outcome

The JS script gets run.

### 3) Input in a specific format breaks the site. (Related with 1)

Input in the following format: **"some_string \<other_string"** to any field breaks the site.

some_string - A 'regular, expected' string.
other_string - Any sequence of characters not containing '\>'

Results in nothing else being shown after some_string. (Interpreted as malformed HTML.)

#### Example

##### Input
- First Name: **Travis \<asdf**
- Last Name: *empty*

##### Outcome

Results in the output **"Welcome Travis"**.

### 4) Empty input results in nothing being output.

**Note:** This behavior may be correct, specification needed.

### 5) When only a First Name is input, there is an additional whitespace between the first name and the period at the end of the first sentence.

### 6) index.php recommendations

- Improve formatting for better readability (might only be the result of my viewer mode, but I cannot see any apparent formatting to speak of)
- Remove unnecessary, non-related comments ( \<!-- I hate my job!!! --\>)

### 7) Unicode Translation

##### Input

The sequence: **\&#116;\&#101;\&#115;\&#116;** into any field.

##### Output

It gets translated into **"test"** upon output, meaning it has to get interpreted somewhere.